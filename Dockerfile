FROM php:5.6.7-apache

MAINTAINER Andrew Munsell <andrew@wizardapps.net>

RUN apt-get update -qq
RUN apt-get install git curl postgresql-client libpq-dev -y -qq

# Install the required PHP extensions
RUN apt-get install php5-xcache -y -qq
RUN mv /usr/lib/php5/20131226/xcache.so /usr/local/lib/php/extensions/no-debug-non-zts-20131226
RUN mv /etc/php5/mods-available/xcache.ini /usr/local/etc/php/conf.d/ext-xcache.ini
RUN php5enmod xcache

RUN pecl install -o -f xdebug \
    && rm -rf /tmp/pear \
    && echo "zend_extension=/usr/local/lib/php/extensions/no-debug-non-zts-20131226/xdebug.so" > /usr/local/etc/php/conf.d/xdebug.ini
RUN php5enmod xdebug

RUN apt-get install libmcrypt-dev -y -qq
RUN docker-php-ext-install mcrypt

RUN docker-php-ext-install mbstring

RUN apt-get install libzip-dev -y -qq
RUN docker-php-ext-install zip

RUN docker-php-ext-install pdo_mysql
RUN docker-php-ext-install pdo_pgsql

RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer
